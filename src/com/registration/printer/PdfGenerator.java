package com.registration.printer;

import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.registration.data.RegistrationInfo;
import com.registration.datastore.RegistrationDatabase;

public class PdfGenerator {

	private String message;
	private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
			Font.BOLD);
	private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
			Font.NORMAL, BaseColor.RED);
	private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
			Font.BOLD);

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private static void addMetaData(Document document, String key) {
		document.addTitle("Alien Registration");
		document.addSubject("Document Registration Number :" + key);

		document.addAuthor("Registartion Admin : Shubham Mittal");
		document.addCreator("Shubham Mittal");
	}

	private static void addEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

	private static void addTitlePage(Document document, String key)
			throws DocumentException {
		Paragraph preface = new Paragraph();

		addEmptyLine(preface, 1);

		preface.add(new Paragraph("Earth Registration Receipt : " + key,
				catFont));

		addEmptyLine(preface, 1);
		document.add(preface);

	}

	public String detailsReaderPdf(String key,
			RegistrationDatabase registrationDatabase) {

		if (registrationDatabase != null) {

			HashMap<String, Object> hashMap = new HashMap<String, Object>();

			hashMap = registrationDatabase.getHashmap();

			RegistrationInfo registrationInfo = (RegistrationInfo) hashMap
					.get(key);

			try {
				Document document = new Document();
				String FILE = "./" + key + ".pdf";
				PdfWriter.getInstance(document, new FileOutputStream(FILE));
				document.open();
				addMetaData(document, key);
				addTitlePage(document, key);

				Paragraph paragraph = new Paragraph();

				paragraph.setAlignment(Element.ALIGN_CENTER);
				paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
				paragraph.add(new Paragraph("Code Name : "
						+ registrationInfo.getCodeName(), smallBold));
				paragraph.add(new Paragraph("Blood Color : "
						+ registrationInfo.getBloodColor(), smallBold));
				paragraph.add(new Paragraph("Registered No. Of Antennas : "
						+ registrationInfo.getAntennasCount(), smallBold));
				paragraph.add(new Paragraph("Registered No. Of Legs : "
						+ registrationInfo.getLegsCount(), smallBold));
				paragraph.add(new Paragraph("Duration Of Stay : "
						+ registrationInfo.getDurationOfStay(), smallBold));
				paragraph.add(new Paragraph("Home Planet : "
						+ registrationInfo.getHomePlanet(), smallBold));
				addEmptyLine(paragraph, 18);
				paragraph
						.add(new Paragraph(
								"Report generated by : Shubham Mittal " + ", " + new Date(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
								smallBold));

				paragraph
						.add(new Paragraph(
								"This Document is a legal identification proof for this creature. This enable him to enjoy all the rights decided under Section 2050 "));

				addEmptyLine(paragraph, 3);

				paragraph
						.add(new Paragraph(
								"This document is authorized and maintaind by Department of Extra-Terrestrial Affairs",
								redFont));

				document.add(paragraph);

				document.close();
				this.setMessage("File Created");
			} catch (Exception e) {
				this.setMessage("File Not Created");
				return this.getMessage();
			}

		}
		return this.getMessage();

	}

}
